"use strict";
function getData(value) {
    return value;
}
console.log(getData("Butga").length);
//ini masalahnya kalo kita pake tipe data any. si number tidak bisa dihitung
console.log(getData(123).length);
//Generic
function myData(value) {
    return value;
}
console.log(myData("Butga").length);
//ini ga akan muncul sugesti buat length, jadi aman kita gabakal pakai length
console.log(myData(123));
// Generic pada Arrow Function buat ts
var arrowFunc = function (value) {
};
// Generic pada Arrow Function buat JSX
//1
// const arrowFunc = <T, >(value: T) => {
// }
//2
// const arrowFunc = <T extends unknown>(value: T) => {
// }
