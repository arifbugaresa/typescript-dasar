"use strict";
//enum
//numeric enum
var Month;
(function (Month) {
    Month[Month["JAN"] = 0] = "JAN";
    Month[Month["FEB"] = 1] = "FEB";
    Month[Month["MAR"] = 2] = "MAR";
    Month[Month["APR"] = 3] = "APR";
    Month[Month["MAY"] = 4] = "MAY";
})(Month || (Month = {}));
//console.log(Month);
// string enums
var Bulan;
(function (Bulan) {
    Bulan["JAN"] = "Januari";
    Bulan["FEB"] = "Februari";
    Bulan["MAR"] = "Maret";
    Bulan["APR"] = "April";
    Bulan["MAY"] = "Mei";
})(Bulan || (Bulan = {}));
console.log(Bulan.FEB);
