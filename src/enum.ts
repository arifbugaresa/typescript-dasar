//enum

//numeric enum
enum Month {
    JAN,
    FEB,
    MAR,
    APR,
    MAY
}

//console.log(Month);

// string enums
enum Bulan {
    JAN = "Januari",
    FEB = "Februari",
    MAR = "Maret",
    APR = "April",
    MAY = "Mei"
}

console.log(Bulan.FEB);