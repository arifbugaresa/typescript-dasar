// String
let nama: string = "Arif";
nama = "Title";

// Number
let umur: number = 20;

//boolean
let isMarried: boolean;
isMarried = false;

//tipe data any untuk semua tipe
let heroes: any;
heroes = 20;
heroes = "Iron Man"
heroes = [];

//union type

let phone: number | string;

phone = 6289809890;
phone = "08232323232";


