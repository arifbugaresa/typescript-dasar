function getData(value: any) {
    return value;
}

console.log(getData("Butga").length);

//ini masalahnya kalo kita pake tipe data any. si number tidak bisa dihitung
console.log(getData(123).length);



//Generic
function myData<Type>(value: Type) {
    return value;
}

console.log(myData("Butga").length);
//ini ga akan muncul sugesti buat length, jadi aman kita gabakal pakai length
console.log(myData(123));

// Generic pada Arrow Function buat ts
const arrowFunc = <T>(value: T) => {

}

// Generic pada Arrow Function buat JSX
//1
// const arrowFunc = <T, >(value: T) => {
    
// }

//2
// const arrowFunc = <T extends unknown>(value: T) => {
    
// }